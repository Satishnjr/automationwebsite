package stepDefinition;

import modules.SelectDressesandProceedtoCheckout;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import pageobjects.AutomationHomePage;
import step_definitions.Hooks;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ProductCheckout {
	WebDriver driver;

	public ProductCheckout() {
		System.out.println("calling constructor");
		driver = Hooks.driver;
		System.out.println(driver);

	}

	@When("^I open automationpractice website$")
	public void i_open_automationpractice_website() throws Throwable {
		System.out.println(driver);

		driver.get("http://automationpractice.com");

	}

	@And("^I select a dresses$")
	public void i_select_a_dresses() throws Throwable {
		PageFactory.initElements(driver, AutomationHomePage.HeaderPage.class);
		SelectDressesandProceedtoCheckout.Execute(driver);

	}

	@And("^I select evening dresses$")
	public void i_select_evening_dresses() throws Throwable {

	}

	@And("^I select a printed dress$")
	public void i_select_a_printed_dress() throws Throwable {

	}

	@Then("^I select pink colour and proceed to checkout$")
	public void i_select_pink_colour_and_proceed_to_checkout() throws Throwable {

	}

}
