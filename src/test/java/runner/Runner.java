package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "features",glue={"stepDefinition"},
plugin = {"pretty", "html:target/cucumber-html-report","json:cucumber.json"},
tags = {}
)
public class Runner {

}
  